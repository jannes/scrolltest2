//
//  AppDelegate.h
//  ScrollTest2
//
//  Created by Jannes on Nov/27/13.
//  Copyright (c) 2013 Jannes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
